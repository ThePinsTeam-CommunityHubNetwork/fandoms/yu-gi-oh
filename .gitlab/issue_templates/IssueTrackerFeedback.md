<!-

General issue tracker feedback template

Please fill up this template for feedback before submitting. 

-->

## Summary
Your feedback summary in less than 240 characters. 

## Explain why. 
Now, strech out about your idea.

## Categories
Don't edit this section, unless you need to add some extras.

~topics::feedback ~"fandoms:Yu-Gi-Oh"

