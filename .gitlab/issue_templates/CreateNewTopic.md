<!--

We recommend to use this issue template in creating new topics here.

Before you start writing, search around the Issue Tracker to avoid duplicates. 

-->

Your content should be placed here, before the `Topic Metadata` section. 

## Topic Metadata
We are using YAML for indexing our Issue Trackers for archival purposes. In case your issue/topic was featured, we'll look up them here as a reference. 
```yaml
## Use YAML formating on metadata and avoid using JSON. 
topic_name: # Same as your issue title.
labels: [
    # Must sync with the tags section.
    "fandoms::Yu-Gi-Oh",
    "topics::Yu-Gi-Oh/your-idea-here"
]
summary: # less than 240 character in plaintext, no Markdown formatting
```
### Tags
Don't edit this section, unless you need to add some more labels. It shoud be synchorized with the values in `labels` key above.

~topics::Yu-Gi-Oh/your-idea-here ~"fandoms:Yu-Gi-Oh"

