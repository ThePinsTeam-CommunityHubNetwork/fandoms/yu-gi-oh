# Fandom Issue Tracker - Yu-Gi-Oh!
You are here in the `gitlab.com:ThePinsTeam-CommunityHubNetwork/fandoms/yu-gi-oh` repository, which are part of [the Pins team's Community network for GitLab](https://en.handbooksbythepins.gq/community-hub/networks/gitlab).

## Purposes of this repository
The purposes of this repository are:
- to have a (not really a official) issue tracker for Yu-Gi-Oh fans.
- to house documentation files for this fandom. [^1]

[^1]: Our own documentation will be not only crowdsourced, but also extensive. Currently WIP.

## Maintainers
| Maintainers | GitLab Username | Maintainer since | Maintainer Status
| ----- | ----- | ----- | -----
| Andrei Jiroh | @AndreiJirohHaliliDev2006 | The early days of this repo | **Active/The Pins Team Staff**

### Apply for maintainership
See `source/maintainership/apply.md` in the [Network Website repository](https://gitlab.com/ThePinsTeam-CommunityHubNetwork/thepinsteam-communityhubnetwork.gitlab.io/blob/master/source/maintainership/apply.md) or in [our English Handbook](https://en.handbooksbythepins.gq/community-hub/fandoms/issue-trakcers/maintainerships#application) for details on how to join the maintainership squad.

## Contributing Policies
See our `CONTRIBUTING.md` for details about how you can contribute here.
